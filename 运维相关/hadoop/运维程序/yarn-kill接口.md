# yarn kill接口

默认的yarn kill任务，是需要登录到机器上执行 yarn -kill 命令来实现。

为了方便调用，可以将此进行一个简易接口化。例如用户打开页面，输入ip，即可完成kill。

或者该接口也是一个基础维护接口，也可以与需要进行任务管理的功能对接。


