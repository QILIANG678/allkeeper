# hdfs监控指标

> 只进行监控，后续产生的衍生操作请参阅 运维相关 中 各组件的 运维程序

## 介绍

## 监控指标

- hdfs 基础 （基础进程、存活节点、jmx对接）
- hdfs rpc延迟 （hdfs中nn与dn之间的rpc通信平均延时时间）
- hdfs namnode基础监控 （堆内存、线程数、gc）
- hdfs 总存储 （存储空间，block数量）
- hdfs 节点详细指标 （列表方式显示nn 和 dn信息，显示物理存储占用、比例、bloack分布）
- hdfs 目录统计 （定时监控各目录大小 有黑白名单之分）
- hdfs 生命周期监控 （基于fsimage进行生命周期监控，小时、天粒度更新各目录生命周期）

## 指标讲解 与 运维对接

## 实现

## 参考