# sqoop监控指标

> 只进行监控，后续产生的衍生操作请参阅 运维相关 中 各组件的 运维程序

## 介绍

## 监控指标

- sqoop 基础 （以任务为主，获取当天产生的sqoop任务主要包括脚本命令、yarn任务、最终同步成功目录）
- sqoop 库表 （以库表为主，获取所有同步中库或表的同步数据量、开始结束时间、同步耗时）

## 指标讲解 与 运维对接

## 实现

## 参考