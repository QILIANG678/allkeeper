# zookeeper监控指标

> 只进行监控，后续产生的衍生操作请参阅 运维相关 中 各组件的 运维程序

## 介绍

## 监控指标

- zookeeper 基础 （cpu、内存、存活节点、jmx、gc）
- 文件描述符
- 平均、最大、最小延迟
- watch数量
- znode数量
- ephemerals数量
- 存储数据量
- leader follower状态
- 发送、接收量
- 日志存储量、文件切分数量

## 指标讲解 与 运维对接

## 实现

## 参考

